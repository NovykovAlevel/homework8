package com.novykov.fastfood.food;

public abstract class Burger {
    private int breadConsumption;
    private int meatConsumption;
    private int cheeseConsumption;
    private int sauceConsumption;
    private int friConsumption;


    public Burger(int breadConsumption, int meatConsumption, int cheeseConsumption, int sauceConsumption) {
        this.breadConsumption = breadConsumption;
        this.meatConsumption = meatConsumption;
        this.cheeseConsumption = cheeseConsumption;
        this.sauceConsumption = sauceConsumption;
        this.friConsumption = friConsumption;

    }

    public int getBreadConsumption() {
        return breadConsumption;
    }

    public int getMeatConsumption() {
        return meatConsumption;
    }

    public int getCheeseConsumption() {
        return cheeseConsumption;
    }

    public int getSauceConsumption() {
        return sauceConsumption;
    }

    public int getFriConsumption() {
        return friConsumption;
    }

    public abstract void create();
}
