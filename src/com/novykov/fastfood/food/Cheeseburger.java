package com.novykov.fastfood.food;

public class Cheeseburger extends Burger {
    public static final int BREAD_CONSUMPTION = 2;
    public static final int MEAT_CONSUMPTION = 1;
    public static final int SAUCE_CONSUMPTION = 1;
    public static final int CHEESE_CONSUMPTION = 1;

    public Cheeseburger() {
        super(BREAD_CONSUMPTION, MEAT_CONSUMPTION, CHEESE_CONSUMPTION, SAUCE_CONSUMPTION);
    }

    @Override
    public void create() {
        System.out.println("Cheeseburger created");
    }
}
