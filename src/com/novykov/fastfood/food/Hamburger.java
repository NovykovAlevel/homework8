package com.novykov.fastfood.food;

public class Hamburger extends Burger {
    public static final int BREAD_CONSUMPTION = 2;
    public static final int MEAT_CONSUMPTION = 1;
    public static final int SAUCE_CONSUMPTION = 1;


    public Hamburger() {
        super(BREAD_CONSUMPTION, MEAT_CONSUMPTION, 0, SAUCE_CONSUMPTION);
    }

    @Override
    public void create() {
        System.out.println("Hamburger created");
    }
}
