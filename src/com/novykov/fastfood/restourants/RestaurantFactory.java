package com.novykov.fastfood.restourants;

import com.novykov.fastfood.restourants.burgerking.BurgerKingFactory;
import com.novykov.fastfood.restourants.mcdonalds.McDonaldsFactory;

public class RestaurantFactory {
    private BurgerKingFactory burgerKingFactory;
    private McDonaldsFactory mcDonaldsFactory;

    public RestaurantFactory() {
        burgerKingFactory = new BurgerKingFactory();
        mcDonaldsFactory = new McDonaldsFactory();
    }

    public Restaurant createRestaurant(RestaurantType type) {
        switch (type) {
            case MCDONALDS:
                return mcDonaldsFactory.createNewMcDonalds();
            case BURGER_KING:
                return burgerKingFactory.createNewBurgerKing();
        } throw new RestorunNotDefined();
    }


    public enum RestaurantType {
        MCDONALDS, BURGER_KING
    }
    public class RestorunNotDefined extends RuntimeException{

    }
}