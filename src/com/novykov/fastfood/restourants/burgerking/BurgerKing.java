package com.novykov.fastfood.restourants.burgerking;

import com.novykov.fastfood.food.Burger;
import com.novykov.fastfood.food.Cheeseburger;
import com.novykov.fastfood.food.Hamburger;
import com.novykov.fastfood.restourants.Restaurant;
import com.novykov.fastfood.warehouse.Warehouse;

import java.util.HashMap;

public class BurgerKing extends Restaurant {


    private static final int TABLES = 20;

    public BurgerKing() {
        super(TABLES);
        setWarehouse(new Warehouse(30, 90, 30, 40, 0));
        createMenu();
    }

    private void createMenu() {
        HashMap<String, Integer> menu = new HashMap<>();
        menu.put("Hamburger", 70);
        menu.put("Cheeseburger", 90);
        setMenu(menu);
    }

    @Override
    public int getOrderPrice(Burger burger, boolean isNeedFri) {
        int price = -1;
        if (isCanCompleteOrder(burger, false)) {
            if (burger instanceof Hamburger) {
                price = getMenu().get("Hamburger");
            } else if (burger instanceof Cheeseburger) {
                price = getMenu().get("Cheeseburger");
            }
        }
        return price;
    }

    @Override
    public void makeOrder(Burger burger, boolean isNeedFri) {
        System.out.println("Make order in BurgerKing");
        completeOrder(burger, isNeedFri);
    }
}
