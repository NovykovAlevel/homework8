package com.novykov.fastfood.restourants;


import com.novykov.fastfood.food.Burger;
import com.novykov.fastfood.warehouse.Warehouse;

import java.util.HashMap;

public abstract class Restaurant {
    private int tablesAmount;
    private boolean isKitchenStoveWorking = true;
    private Warehouse warehouse;

    HashMap<String, Integer> menu = new HashMap<>();

    public Restaurant(int tablesAmount) {
        this.tablesAmount = tablesAmount;
    }

    protected void setWarehouse(Warehouse warehouse) {
        this.warehouse = warehouse;
    }

    protected void setMenu(HashMap<String, Integer> menu) {
        this.menu = menu;
    }

    protected HashMap<String, Integer> getMenu() {
        return menu;
    }

    protected boolean isCanCompleteOrder(Burger burger, boolean isNeedFri) {
        return isKitchenStoveWorking && isHasFreeTable() && isHasIngridients(burger, isNeedFri);
    }

    private boolean isHasFreeTable() {
        boolean isTablesEnought = tablesAmount > 0;
        if (!isTablesEnought) {
            System.out.println("Can't make order: free tables is over.");
        }
        return isTablesEnought;
    }

    private boolean isHasIngridients(Burger burger, boolean isNeedFri) {
        return warehouse.isHasIngridients(burger,isNeedFri);
    }

    protected void completeOrder(Burger burger, boolean isNeedFri) {
        if(isNeedFri){
            warehouse.friCreated();
        }
        warehouse.burgerCreated(burger);
        tablesAmount--;
    }

    public abstract int getOrderPrice(Burger burger, boolean isNeedFri);
    public abstract void makeOrder(Burger burger, boolean isNeedFri);
}
