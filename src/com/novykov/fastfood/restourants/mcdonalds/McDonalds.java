package com.novykov.fastfood.restourants.mcdonalds;

import com.novykov.fastfood.food.Burger;
import com.novykov.fastfood.food.Cheeseburger;
import com.novykov.fastfood.food.Hamburger;
import com.novykov.fastfood.restourants.Restaurant;
import com.novykov.fastfood.warehouse.Warehouse;

import java.util.HashMap;

public class McDonalds extends Restaurant {

    private static final int TABLES = 20;

    public McDonalds() {
        super(TABLES);
        setWarehouse(new Warehouse(200, 100, 50, 70, 40));
        createMenu();
    }

    private void createMenu() {
        HashMap<String,Integer> menu = new HashMap<>();
        menu.put("Hamburger",50);
        menu.put("Cheeseburger", 65);
        menu.put("Hamburger menu", 60);
        menu.put("Cheeseburger menu", 75);
        setMenu(menu);
    }

    @Override
    public int getOrderPrice(Burger burger, boolean isNeedFri) {
        int price = -1;
        if (isCanCompleteOrder(burger, isNeedFri)) {
            if (burger instanceof Hamburger) {
                if (isNeedFri) {
                    price = getMenu().get("Hamburger menu");
                } else {
                    price = getMenu().get("Hamburger");
                }
            } else if (burger instanceof Cheeseburger) {
                if (isNeedFri) {
                    price = getMenu().get("Cheeseburger menu");
                } else {
                    price = getMenu().get("Cheeseburger");
                }
            }
        }
        return price;
    }
    @Override
    public void makeOrder(Burger burger, boolean isNeedFri) {
        System.out.println("Make order in mcdonalds");
         completeOrder(burger, isNeedFri);
    }
}
