package com.novykov.fastfood.client;

import com.novykov.fastfood.food.Burger;

public class Client {

    private Burger burger;
    private boolean isNeedFri;

    private int cashBalance;
    private int cardBalance;

    public Client(int cashBalance, int cardBalance) {
        this.cashBalance = cashBalance;
        this.cardBalance = cardBalance;
    }

    public int getCashBalance() {
        return cashBalance;
    }

    public int getCardBalance() {
        return cardBalance;
    }

    public Burger getBurger() {
        return burger;
    }

    public void setBurger(Burger burger) {
        this.burger = burger;
    }

    public boolean isNeedFri() {
        return isNeedFri;
    }

    public void setNeedFri(boolean needFri) {
        isNeedFri = needFri;
    }

    public void setCashBalance(int cashBalance) {
        this.cashBalance = cashBalance;
    }

    public void setCardBalance(int cardBalance) {
        this.cardBalance = cardBalance;
    }

    public PaymentType getPaymentType(int price) {
        if (isCashBalanceEnough(price)) {
            System.out.println("Payment type - Cash");
            return PaymentType.CASH;
        } else if (isCardBalanceEnough(price)) {
            System.out.println("Payment type - Card");
            return PaymentType.CARD;
        }
        System.out.println("Not enough money!");
        return null;
    }

    public void isPaySuccess(PaymentType type, int price) {
        switch (type) {
            case CASH:
                cashBalance = cashBalance - price;
                System.out.println("Payment successfully made. Cash balance: " + cashBalance);
                break;

            case CARD:
                cardBalance = cardBalance - price;
                System.out.println("Payment successfully made. Card balance: " + cardBalance);
                break;
        }
    }

    private boolean isCashBalanceEnough(int price) {
        return cashBalance > price;
    }

    private boolean isCardBalanceEnough(int price) {
        return cardBalance > price;
    }
}
