package com.novykov.fastfood.client;

public enum PaymentType {
    CARD, CASH
}
