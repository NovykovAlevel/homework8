package com.novykov.fastfood.warehouse;

import com.novykov.fastfood.food.Burger;

public class Warehouse {

    private int breadAmount;
    private int meatAmount;
    private int cheeseAmount;
    private int sauceAmount;
    private int friAmount;

    public Warehouse(int breadAmount, int meatAmount, int cheeseAmount, int sauceAmount, int friAmount) {
        this.breadAmount = breadAmount;
        this.meatAmount = meatAmount;
        this.cheeseAmount = cheeseAmount;
        this.sauceAmount = sauceAmount;
        this.friAmount = friAmount;
    }

    public boolean isHasIngridients(Burger burger, boolean isNeedFri) {
        boolean isHasIngridients = true;
        if (isNeedFri) {
            isHasIngridients = isFriEnough();
        }
        isHasIngridients =
                isBreadEnough(burger.getBreadConsumption())
                        && isCheeseEnough(burger.getCheeseConsumption())
                        && isMeatEnough(burger.getMeatConsumption())
                        && isSauceEnough(burger.getSauceConsumption());

        return isHasIngridients;
    }

    public void burgerCreated(Burger burger) {
        breadAmount = breadAmount - burger.getBreadConsumption();
        meatAmount = meatAmount - burger.getMeatConsumption();
        cheeseAmount = cheeseAmount - burger.getCheeseConsumption();
        sauceAmount = sauceAmount - burger.getSauceConsumption();
    }

    public void friCreated() {
        friAmount--;
    }

    private boolean isBreadEnough(int count) {
        boolean breadEnough = breadAmount - count > 0;
        if (!breadEnough) {
            System.out.println("Not enough bread");
        }
        return breadEnough;
    }

    private boolean isMeatEnough(int count) {
        boolean meatEnough = meatAmount - count > 0;
        if (!meatEnough) {
            System.out.println("Not enough meat");
        }
        return meatEnough;
    }

    private boolean isCheeseEnough(int count) {
        boolean cheeseEnough = cheeseAmount - count > 0;
        if (!cheeseEnough) {
            System.out.println("Not enough cheese");
        }
        return cheeseEnough;
    }

    private boolean isSauceEnough(int count) {
        boolean sauceEnough = sauceAmount - count > 0;
        if (!sauceEnough) {
            System.out.println("Not enough sauce");
        }
        return sauceEnough;
    }

    private boolean isFriEnough() {
        boolean friEnough = friAmount-- > 0;
        if (!friEnough) {
            System.out.println("Not enough fri");
        }
        return friEnough;
    }
}