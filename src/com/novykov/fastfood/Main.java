package com.novykov.fastfood;

import com.novykov.fastfood.client.Client;
import com.novykov.fastfood.client.PaymentType;
import com.novykov.fastfood.food.Burger;
import com.novykov.fastfood.food.Cheeseburger;
import com.novykov.fastfood.food.Hamburger;
import com.novykov.fastfood.restourants.Restaurant;
import com.novykov.fastfood.restourants.RestaurantFactory;

import java.util.ArrayList;
import java.util.Random;

import static com.novykov.fastfood.restourants.RestaurantFactory.RestaurantType.BURGER_KING;
import static com.novykov.fastfood.restourants.RestaurantFactory.RestaurantType.MCDONALDS;

public class Main {
    public static final int AMOUNT_CLIENTS = 50;

    public static void main(String[] args) {
        RestaurantFactory restaurantFactory = new RestaurantFactory();
        Restaurant mcdonalds = restaurantFactory.createRestaurant(MCDONALDS);
        Restaurant burgerKing = restaurantFactory.createRestaurant(BURGER_KING);
        ArrayList<Client> clients = createClients();
        for (Client client : clients) {
            System.out.println("****************");
            boolean isMcDonalds = new Random().nextBoolean();
            if (isMcDonalds) {
                System.out.println("McDonalds");
                if (!makeOrder(mcdonalds, client)) {
                    System.out.println("BurgerKing");
                    makeOrder(burgerKing, client);
                }
            } else {
                if (client.isNeedFri()) {
                    System.out.println("McDonalds");
                    if (!makeOrder(mcdonalds, client)) {
                        System.out.println("Sorry we are not sell fri!");
                    }
                } else {
                    System.out.println("BurgerKing");
                    if (!makeOrder(burgerKing, client)) {
                        System.out.println("McDonalds");
                        makeOrder(mcdonalds, client);
                    }
                }
            }
        }
    }

    private static boolean makeOrder(Restaurant restaurant, Client client) {
        System.out.printf("User with cash balance %d | card balance %d, wants burger: %s %s\n",
                client.getCashBalance(), client.getCardBalance(), client.getBurger().getClass().getSimpleName(), client.isNeedFri() ? " with fri" : "");
        int price = restaurant.getOrderPrice(client.getBurger(), client.isNeedFri());
        if (price > 0) {
            System.out.println(price);
            PaymentType type = client.getPaymentType(price);

            if (type != null) {
                restaurant.makeOrder(client.getBurger(), client.isNeedFri());
                client.isPaySuccess(type, price);
                return true;
            }
        }
        return false;
    }

    private static ArrayList<Client> createClients() {
        ArrayList<Client> clients = new ArrayList<>();
        for (int i = 0; i < AMOUNT_CLIENTS; i++) {
            Burger burger = getBurger(new Random().nextInt(2));
            Client client = new Client(new Random().nextInt(100), new Random().nextInt(200));
            client.setBurger(burger);
            client.setNeedFri(new Random().nextBoolean());
            clients.add(client);
        }
        return clients;
    }

    private static Burger getBurger(int x) {
        switch (x) {
            case 0:
                return new Hamburger();

            case 1:
            default:
                return new Cheeseburger();
        }
    }

}
